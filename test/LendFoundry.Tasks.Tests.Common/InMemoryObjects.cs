﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.MoneyMovement.Ach.Configuration;
using LendFoundry.Notifications.Client;
using LendFoundry.Security.Tokens;
using Moq;

namespace LendFoundry.Tasks.Tests.Common
{
    public abstract class InMemoryObjects
    {
        protected string TenantId { get; } = "my-tenant";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();
        protected Mock<IConfigurationService<AchConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<AchConfiguration>>();

        // ------------------------- TenantTime Objects
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();

        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        // ------------------------- Loan Objects
        protected Mock<ILoanService> LoanService { get; } = new Mock<ILoanService>();
        protected Mock<ILoanServiceFactory> LoanServiceFactory { get; } = new Mock<ILoanServiceFactory>();

        // ------------------------- Calendar Objects
        protected Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>();
        protected Mock<ICalendarServiceFactory> CalendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();

        protected Mock<INotificationServiceFactory> NotificationFactory { get; } = new Mock<INotificationServiceFactory>();

        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        // ------------------------- Ach Objects
        protected Mock<IAchService> AchService { get; } = new Mock<IAchService>();
        protected Mock<IAchServiceFactory> AchServiceFactory { get; } = new Mock<IAchServiceFactory>();
    }
}