﻿using Address = LendFoundry.Loans.Address;
using Borrower = LendFoundry.Loans.Borrower;
using Fee = LendFoundry.Loans.Fee;
using Installment = LendFoundry.Loans.Client.Models.Installment;
using LendFoundry.Loans.Client.Models;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans;
using Loan = LendFoundry.Loans.Loan;
using LoanDetails = LendFoundry.Loans.LoanDetails;
using LoanTerms = LendFoundry.Loans.LoanTerms;
using Phone = LendFoundry.Loans.Phone;
using System.Collections.Generic;
using System;
using LendFoundry.Configuration.Client;
using Moq;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using LendFoundry.Loans.Client;
using LendFoundry.Notifications.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tasks.Loans.CheckPaymentDueNotification.Configuration;
using LendFoundry.Calendar.Client;
using LendFoundry.Loans.Payment;
using LendFoundry.Loans.Payment.Client;

namespace LendFoundry.Tasks.Tests
{
    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "rockloans";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();
        protected Mock<IConfigurationService<CheckPaymentDueConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<CheckPaymentDueConfiguration>>();

        // ------------------------- TenantTime Objects
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();
        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();

        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        // ------------------------- Loan Objects
        protected Mock<ILoanService> LoanService { get; } = new Mock<ILoanService>();
        protected Mock<ILoanServiceFactory> LoanServiceFactory { get; } = new Mock<ILoanServiceFactory>();

        // ------------------------- Calendar Objects
        protected Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>();
        protected Mock<ICalendarServiceFactory> CalendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();

        protected Mock<INotificationServiceFactory> NotificationFactory { get; } = new Mock<INotificationServiceFactory>();

        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        // ------------------------- Payment Objects
        protected Mock<IPaymentService> PaymentService { get; } = new Mock<IPaymentService>();
        protected Mock<IPaymentServiceClientFactory> PaymentServiceFactory { get; } = new Mock<IPaymentServiceClientFactory>();

        protected Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>();

        public IInstallmentInfo InMemoryInstallmentInfo
        {
            get
            {
                var installmentInfo = new LendFoundry.Loans.InstallmentInfo
                {
                    Loan = GetLoan(string.Empty),
                    Installment = Installment
                };

                return installmentInfo;
            }
        }

        private static IInstallment Installment => new Installment
        {
            AnniversaryDate = new DateTime(2016, 05, 23),
            DueDate = new DateTime(2016, 05, 23),
            EndingBalance = 668.95,
            OpeningBalance = 1000.00,
            PaymentMethod = PaymentMethod.ACH,
            Expected = Mock.Of<IPaydown>(p => p.Amount == 337.92 && p.Interest == 6.87 && p.Principal == 331.05),
            Status = InstallmentStatus.Processing,
            Type = InstallmentType.Installment
        };

        public static ILoanDetails GetLoanDetails(string loanReferenceNumber)
        {
            var loanInfo = GetLoanInfo(loanReferenceNumber);
            var loanAdapter = new InMemoryLoanAdapter(loanInfo);

            var loanDetails = new LoanDetails
            {
                LoanInfo = loanAdapter,
                Transactions = GetTransactions(),
                PaymentSchedule = GetPaymentSchedule(loanReferenceNumber)
            };

            return loanDetails;
        }
        
        public static ILoan GetLoan(string referenceNumber = null)
        {
            return
                new Loan
                {
                    BankAccounts = new List<IBankAccount>
                    {
                        new LendFoundry.Loans.Client.Models.BankAccount
                        {
                            AccountNumber = "219",
                            AccountType = BankAccountType.Checking,
                            RoutingNumber = "001",
                            BankName = "BankName",
                            IsPrimary = true
                        }
                    },
                    Borrowers = new[]
                    {
                        new Borrower
                        {
                            ReferenceNumber = "0001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "F",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                            SSN = "103-58-4851",
                            Email = "johndoe@lendfoundry.com",
                            Addresses = new[]
                            {
                                new Address
                                {
                                    IsPrimary = true,
                                    Line1 = "145 5th Ave",
                                    City = "Irvine",
                                    State = "CA",
                                    ZipCode = "12952"
                                }
                            },
                            Phones = new[]
                            {
                                new Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "23938439"
                                }
                            }
                        }
                    },
                    CampaignCode = "190C-2",
                    FundingSource = "crb",
                    Grade = "B",
                    HomeOwnership = "Owner",
                    Investor = new LoanInvestor
                    {
                        Id = "1001",
                        LoanPurchaseDate = new DateTimeOffset(DateTime.Now)
                    },
                    MonthlyIncome = 7000,
                    PaymentMethod = PaymentMethod.ACH,
                    PostCloseDti = 6.4,
                    PreCloseDti = 5.7,
                    Purpose = "Home improvement",
                    ReferenceNumber = referenceNumber,
                    Scores = new[]
                    {
                        new Score
                        {
                            Name = "ficoRiskScore09",
                            InitialDate = new DateTimeOffset(DateTime.Now),
                            InitialScore = "500",
                            UpdatedDate = new DateTimeOffset(DateTime.Now),
                            UpdatedScore = "601"
                        }
                    },
                    Terms = new LoanTerms
                    {
                        ApplicationDate = new DateTimeOffset(DateTime.Now),
                        APR = 100,
                        FundedDate = new DateTimeOffset(DateTime.Now),
                        LoanAmount = 1000,
                        OriginationDate = new DateTimeOffset(DateTime.Now),
                        Term = 3,
                        Rate = 8.24,
                        RateType = RateType.Percent,
                        Fees = new[]
                        {
                            new Fee
                            {
                                Amount = 212,
                                Code = "142",
                                Type = FeeType.Fixed
                            }
                        }
                    }
                };
        }

        public static LoanInfo GetLoanInfo(string loanReferenceNumber = null)
        {
            var loanInfo = new LoanInfo
            {
                ReferenceNumber = loanReferenceNumber,
                FundingSource = "crb",
                Purpose = "Home improvement",
                Status = new LendFoundry.Loans.Client.Models.LoanStatus(),
                Investor = new LoanInvestor
                {
                    Id = "1001",
                    LoanPurchaseDate = new DateTimeOffset(DateTime.Now)
                },
                CurrentDue = 2,
                DaysPastDue = 2,
                LastPaymentAmount = 1110,
                LastPaymentDate = new DateTimeOffset(DateTime.Now),
                NextDueDate = new DateTimeOffset(DateTime.Now),
                NextInstallmentAmount = 10,
                RemainingBalance = 10,
                Scores = new[]
                    {
                        new Score
                        {
                            Name = "ficoRiskScore09",
                            InitialDate = new DateTimeOffset(DateTime.Now),
                            InitialScore = "500",
                            UpdatedDate = new DateTimeOffset(DateTime.Now),
                            UpdatedScore = "601"
                        }
                    },
                Grade = "B",
                PreCloseDti = 5.7,
                PostCloseDti = 6.4,
                MonthlyIncome = 7000,
                HomeOwnership = "Owner",
                CampaignCode = "190C-2",
                Borrowers = new[]
                    {
                        new LendFoundry.Loans.Client.Models.Borrower
                        {
                            ReferenceNumber = "0001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "F",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                            SSN = "103-58-4851",
                            Email = "johndoe@lendfoundry.com",
                            Addresses = new[]
                            {
                                new LendFoundry.Loans.Client.Models.Address
                                {
                                    IsPrimary = true,
                                    Line1 = "145 5th Ave",
                                    City = "Irvine",
                                    State = "CA",
                                    ZipCode = "12952"
                                }
                            },
                            Phones = new[]
                            {
                                new LendFoundry.Loans.Client.Models.Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "23938439"
                                }
                            }
                        }
                    },
                Terms = new LendFoundry.Loans.Client.Models.LoanTerms
                {
                    ApplicationDate = new DateTimeOffset(DateTime.Now),
                    APR = 100,
                    FundedDate = new DateTimeOffset(DateTime.Now),
                    LoanAmount = 1000,
                    OriginationDate = new DateTimeOffset(DateTime.Now),
                    Term = 3,
                    Rate = 8.24,
                    RateType = RateType.Percent,
                    Fees = new[]
                        {
                            new LendFoundry.Loans.Client.Models.Fee
                            {
                                Amount = 212,
                                Code = "142",
                                Type = FeeType.Fixed
                            }
                        }
                },
                BankAccounts = new List<LendFoundry.Loans.Client.Models.BankAccount>
                {
                    new LendFoundry.Loans.Client.Models.BankAccount
                    {
                        AccountNumber = "219",
                        AccountType = BankAccountType.Checking,
                        RoutingNumber = "001"
                    }
                }.ToArray(),
                PaymentMethod = PaymentMethod.ACH
            };
            return loanInfo;
        }

        public static IPaymentSchedule GetPaymentSchedule(string loanReferenceNumber = null)
        {
            return
                new PaymentSchedule
                {
                    LoanReferenceNumber = loanReferenceNumber,
                    TenantId = string.Empty,
                    Installments = new[]
                    {
                        new Installment {
                            Expected = Mock.Of<IPaydown>(p => p.Amount == 100.0),
                            DueDate = new DateTimeOffset(DateTime.Now)
                        }
                    }
                };
        }

        public static IPaymentScheduleSummary GetSummary()
        {
            return
                new PaymentScheduleSummary
                {
                    CurrentDue = 2,
                    DaysPastDue = 2,
                    LastPaymentAmount = 1000,
                    LastPaymentDate = new DateTimeOffset(DateTime.Now),
                    NextDueDate = new DateTimeOffset(DateTime.Now),
                    NextInstallmentAmount = 10,
                    RemainingBalance = 1000
                };
        }

        public static List<ITransaction> GetTransactions()
        {
            return new List<ITransaction>
            {
                new Transaction {
                        Amount = 10,
                        Date = new DateTimeOffset(DateTime.Now)
                    }
            };
        }

        public class InMemoryLoanAdapter : ILoanInfo
        {
            public InMemoryLoanAdapter(LoanInfo model)
            {
                Loan = new Loan
                {
                    BankAccounts = new List<LendFoundry.Loans.Client.Models.BankAccount>
                    {
                        new LendFoundry.Loans.Client.Models.BankAccount
                        {
                            AccountNumber = "219",
                            AccountType = BankAccountType.Checking,
                            RoutingNumber = "001"
                        }
                    },
                    Borrowers = new[]
                    {
                        new Borrower
                        {
                            ReferenceNumber = "0001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "F",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                            SSN = "103-58-4851",
                            Email = "johndoe@lendfoundry.com",
                            Addresses = new[]
                            {
                                new Address
                                {
                                    IsPrimary = true,
                                    Line1 = "145 5th Ave",
                                    City = "Irvine",
                                    State = "CA",
                                    ZipCode = "12952"
                                }
                            },
                            Phones = new[]
                            {
                                new Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "23938439"
                                }
                            }
                        }
                    },
                    CampaignCode = "190C-2",
                    FundingSource = "crb",
                    Grade = "B",
                    HomeOwnership = "Owner",
                    Investor = new LoanInvestor
                    {
                        Id = "1001",
                        LoanPurchaseDate = new DateTimeOffset(DateTime.Now)
                    },
                    MonthlyIncome = 7000,
                    PaymentMethod = PaymentMethod.ACH,
                    PostCloseDti = 6.4,
                    PreCloseDti = 5.7,
                    Purpose = "Home improvement",
                    ReferenceNumber = model.ReferenceNumber,
                    Scores = new[]
                    {
                        new Score
                        {
                            Name = "ficoRiskScore09",
                            InitialDate = new DateTimeOffset(DateTime.Now),
                            InitialScore = "500",
                            UpdatedDate = new DateTimeOffset(DateTime.Now),
                            UpdatedScore = "601"
                        }
                    },
                    Terms = new LoanTerms
                    {
                        ApplicationDate = new DateTimeOffset(DateTime.Now),
                        APR = 100,
                        FundedDate = new DateTimeOffset(DateTime.Now),
                        LoanAmount = 1000,
                        OriginationDate = new DateTimeOffset(DateTime.Now),
                        Term = 3,
                        Rate = 8.24,
                        RateType = RateType.Percent,
                        Fees = new[]
                        {
                            new Fee
                            {
                                Amount = 212,
                                Code = "142",
                                Type = FeeType.Fixed
                            }
                        }
                    }
                };

                Summary = new PaymentScheduleSummary
                {
                    CurrentDue = model.CurrentDue,
                    DaysPastDue = model.DaysPastDue,
                    LastPaymentAmount = model.LastPaymentAmount,
                    LastPaymentDate = model.LastPaymentDate,
                    NextDueDate = model.NextDueDate,
                    NextInstallmentAmount = model.NextInstallmentAmount,
                    RemainingBalance = model.RemainingBalance
                };
            }

            public ILoan Loan { get; }

            public IPaymentScheduleSummary Summary { get; }
        }
    }
}