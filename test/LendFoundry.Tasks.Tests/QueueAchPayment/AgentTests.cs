﻿using LendFoundry.Configuration.Client;
using LendFoundry.Security.Tokens;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Calendar.Client;
using Xunit;
using LendFoundry.Loans;
using LendFoundry.Loans.Payment;
using LendFoundry.Tasks.Loans.QueueAchPayment;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Tasks.Tests.QueueAchPayment
{
    public class AgentTests : InMemoryObjects
    {
        private Loans.QueueAchPayment.Agent Agent => new Loans.QueueAchPayment.Agent
            (
                ConfigurationFactory.Object,
                TokenHandler.Object,
                LoanServiceFactory.Object,
                TenantTimeFactory.Object,
                CalendarServiceFactory.Object,
                PaymentServiceFactory.Object,
                Mock.Of<IEventHubClientFactory>(),
                Logger.Object
            );

        [Fact]
        public void QueueAchPayment_OnSchedule_When_ExecutionOk()
        {
            // arrange
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Info(It.IsAny<string>(), It.IsAny<object>()));
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Business, Date = new DateTimeOffset(DateTime.Now) });

            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(CalendarService.Object);

            PaymentService.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()));

            PaymentServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(PaymentService.Object);

            var installments = new[]
            {
                InMemoryInstallmentInfo,
                InMemoryInstallmentInfo,
                InMemoryInstallmentInfo
            }.Select((installment, i) =>
            {
                i++;
                installment.Loan.ReferenceNumber = $"loan00{i}";

                return installment;
            }).ToArray();

            LoanService.Setup(x => x.GetInstallmentsDueIn(It.IsAny<DateTime>(), It.IsAny<PaymentMethod>(), It.IsAny<InstallmentType>()))
                .Returns(installments);

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(LoanService.Object);

            // action
            Agent.OnSchedule();

            // assert
            Logger.Verify(x => x.Info(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeast(installments.Length * 2));
            PaymentServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            PaymentService.Verify(x => x.AddPayment(It.IsAny<IPaymentRequest>()), Times.AtLeast(installments.Length));
        }

        [Fact]
        public void QueueAchPayment_OnSchedule_When_HasNoInstallments()
        {
            // arrange
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Info(It.IsAny<string>(), It.IsAny<object>()));
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Business, Date = new DateTimeOffset(DateTime.Now) });

            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(CalendarService.Object);

            PaymentService.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()));

            PaymentServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(PaymentService.Object);

            var installments = new List<IInstallmentInfo>().ToArray();
            LoanService.Setup(x => x.GetInstallmentsDueIn(It.IsAny<DateTime>(), It.IsAny<PaymentMethod>(), It.IsAny<InstallmentType>()))
                .Returns(installments);

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(LoanService.Object);

            // action
            Agent.OnSchedule();

            // assert
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            PaymentServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Never);
            PaymentService.Verify(x => x.AddPayment(It.IsAny<IPaymentRequest>()), Times.Never);
        }

        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

            //program.Main(null);
        }
    }
}