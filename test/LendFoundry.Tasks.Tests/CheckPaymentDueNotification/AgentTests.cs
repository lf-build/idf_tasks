﻿using INotificationService = LendFoundry.Notifications.Client.INotificationService;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Tasks.Loans.CheckPaymentDueNotification.Configuration;
using Moq;
using System;
using LendFoundry.Security.Tokens;
using Xunit;
using LendFoundry.Tasks.Loans.CheckPaymentDueNotification;

namespace LendFoundry.Tasks.Tests.CheckPaymentDueNotification
{
    public class AgentTests : InMemoryObjects
    {
        private Loans.CheckPaymentDueNotification.Agent Agent => new Loans.CheckPaymentDueNotification.Agent
            (
                ConfigurationFactory.Object,
                TokenHandler.Object,
                LoanServiceFactory.Object,
                NotificationFactory.Object,
                TenantTimeFactory.Object,
                LoggerFactory.Object
            );


        [Fact]
        public void Agent_OnSchedule_When_ExecutionOk()
        {
            // arrange
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Mock.Of<ILogger>());

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<ILoanService>());

            ConfigurationService.Setup(x => x.Get())
                .Returns(new CheckPaymentDueConfiguration
                {
                    CheckProcessingFee = 5,
                    DaysDelayed = 5,
                    TemplateKey = "due-check-payment-reminder"
                });

            ConfigurationFactory.Setup(
                x => x.Create<CheckPaymentDueConfiguration>(
                    It.IsAny<string>(), 
                    It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            NotificationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<INotificationService>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // act
            Agent.OnSchedule();

            // assert
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            LoanServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            NotificationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationFactory.Verify(x => x.Create<CheckPaymentDueConfiguration>(It.IsAny<string>(),It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(2));
        }

        [Fact]
        public void Agent_OnSchedule_When_HasNoConfiguration()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<ILoanService>());

            ConfigurationService.Setup(x => x.Get());

            ConfigurationFactory.Setup(
                x => x.Create<CheckPaymentDueConfiguration>(
                    It.IsAny<string>(),
                    It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            NotificationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<INotificationService>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // act
            Agent.OnSchedule();

            // assert
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            LoanServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            NotificationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationFactory.Verify(x => x.Create<CheckPaymentDueConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.Once);
            TenantTime.Verify(x => x.Today, Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
        }
    

        [Fact]
        public void Agent_OnSchedule_When_HasNoCheckPaymentDueConfig()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<ILoanService>());

            ConfigurationService.Setup(x => x.Get());

            ConfigurationFactory.Setup(
                x => x.Create<CheckPaymentDueConfiguration>(
                    It.IsAny<string>(),
                    It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            NotificationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<INotificationService>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // act
            Agent.OnSchedule();

            // assert
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            LoanServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            NotificationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationFactory.Verify(x => x.Create<CheckPaymentDueConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            TenantTime.Verify(x => x.Today, Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnSchedule_When_HasNoDaysDelayed()
        {
            // arrange
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<ILoanService>());

            ConfigurationService.Setup(x => x.Get())
                .Returns(new CheckPaymentDueConfiguration
                {
                    CheckProcessingFee = 5,
                    DaysDelayed = 0,
                    TemplateKey = "due-check-payment-reminder"
                });

            ConfigurationFactory.Setup(
                x => x.Create<CheckPaymentDueConfiguration>(
                    It.IsAny<string>(),
                    It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            NotificationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(Mock.Of<INotificationService>());

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // act
            Agent.OnSchedule();

            // assert
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            LoanServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            NotificationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationFactory.Verify(x => x.Create<CheckPaymentDueConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.Once);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            TenantTime.Verify(x => x.Today, Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

            //program.Main(null);
        }
    }
}