﻿using INotificationService = LendFoundry.Notifications.Client.INotificationService;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans;
using LendFoundry.Tasks.Loans.CheckPaymentDueNotification.Configuration;
using LendFoundry.Tasks.Loans.CheckPaymentDueNotification;
using Moq;
using System.Linq;
using System;
using Xunit;

namespace LendFoundry.Tasks.Tests.CheckPaymentDueNotification
{
    public class CheckPaymentDueNotificationTests : InMemoryObjects
    {
        private Mock<INotificationService> NotificationService { get; } = new Mock<INotificationService>();

        private NotificationSender NotificantionSender => new NotificationSender
            (
                LoanService.Object,
                NotificationService.Object,
                Logger.Object,
                ConfigurationService.Object
            );



        [Fact]
        public void SendNotifications_When_ExecutionOk()
        {
            // arrange
            var installments = new[]
            {
                InMemoryInstallmentInfo,
                InMemoryInstallmentInfo,
                InMemoryInstallmentInfo
            };
            installments = installments.Select((installment, i) =>
            {
                installment.Loan.ReferenceNumber = $"loan00{i}";
                installment.Loan.PaymentMethod = PaymentMethod.Check;
                installment.Installment.PaymentMethod = PaymentMethod.Check;

                return installment;
            }).ToArray();

            ConfigurationService.Setup(x => x.Get()).Returns(new CheckPaymentDueConfiguration
            {
                CheckProcessingFee = 5,
                DaysDelayed = 5,
                TemplateKey = "due-check-payment-reminder"
            });

            LoanService.Setup(
                x => x.GetInstallmentsDueIn(
                        It.IsAny<DateTime>(),
                        It.IsAny<PaymentMethod>(),
                        It.IsAny<InstallmentType>())).Returns(installments);

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            // act
            NotificantionSender.SendNotifications(It.IsAny<PaymentMethod>(), It.IsAny<DateTimeOffset>());

            // assert
            LoanService.Verify(
                x =>
                    x.GetInstallmentsDueIn(
                        It.IsAny<DateTime>(),
                        It.IsAny<PaymentMethod>(),
                        It.IsAny<InstallmentType>()), Times.Once);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(3));
            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never());
        }


        [Fact]
        public void SendNotifications_When_HasNoConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                ConfigurationService.Setup(x => x.Get());

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                NotificantionSender.SendNotifications(It.IsAny<PaymentMethod>(), It.IsAny<DateTimeOffset>());

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeast(1));
            });
        }

        [Fact]
        public void SendNotifications_When_HasNoCheckProcessingFee()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                ConfigurationService.Setup(x => x.Get()).Returns(new CheckPaymentDueConfiguration
                {
                    CheckProcessingFee = 0,
                    DaysDelayed = 5,
                    TemplateKey = "due-check-payment-reminder"
                });

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                NotificantionSender.SendNotifications(It.IsAny<PaymentMethod>(), It.IsAny<DateTimeOffset>());

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeast(1));
            });
        }

        [Fact]
        public void SendNotifications_When_HasNoTemplateKey()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                ConfigurationService.Setup(x => x.Get()).Returns(new CheckPaymentDueConfiguration
                {
                    CheckProcessingFee = 5,
                    DaysDelayed = 5,
                    TemplateKey = string.Empty
                });

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                NotificantionSender.SendNotifications(It.IsAny<PaymentMethod>(), It.IsAny<DateTimeOffset>());

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeast(1));
            });
        }
    }
}