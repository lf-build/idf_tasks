﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Ach.ProcessAchFile;
using Moq;
using Xunit;

namespace LendFoundry.Tasks.Tests
{
    public class ProcessAchFileTest
    {
        private Mock<IConfigurationServiceFactory> configurationFactory { get; } = new Mock<IConfigurationServiceFactory>();
        private Mock<ITokenHandler> tokenHandler { get; } = new Mock<ITokenHandler>();
        private Mock<ITenantTimeFactory> tenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();
        private Mock<ICalendarServiceFactory> calendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();
        private Mock<IAchServiceFactory> achServiceFactory { get; } = new Mock<IAchServiceFactory>();
        private Mock<ILoggerFactory> loggerFactory { get; } = new Mock<ILoggerFactory>();
        private Mock<IToken> Token { get; } = new Mock<IToken>(); 
        private Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>(); 
        private Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>(); 
        private Mock<IAchService> AchService { get; } = new Mock<IAchService>(); 
        private Mock<ILogger> Logger { get; } = new Mock<ILogger>(); 
        private Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>(); 

        private IDateInfo DateInfo => new DateInfo()
        {
            Type = DateType.Business
        };

        private Ach.ProcessAchFile.Agent agent => new Ach.ProcessAchFile.Agent(
            configurationFactory.Object,
            tokenHandler.Object,
            tenantTimeFactory.Object,
            calendarServiceFactory.Object,
            achServiceFactory.Object,
            loggerFactory.Object.CreateLogger()
            );


        [Fact]
        public void OnSchedule_Test()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            tokenHandler.Setup(a => a.Issue(It.IsAny<string>(), It.IsAny<string>())).Returns(Token.Object);
            tenantTimeFactory.Setup(
                a => a.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<StaticTokenReader>()))
                .Returns(TenantTime.Object);

            calendarServiceFactory.Setup(a => a.Create(It.IsAny<StaticTokenReader>())).Returns(CalendarService.Object);
            CalendarService.Setup(a => a.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(DateInfo);

            achServiceFactory.Setup(a => a.Create(It.IsAny<StaticTokenReader>())).Returns(AchService.Object);

            loggerFactory.Setup(a => a.CreateLogger()).Returns(Logger.Object);

            SettingReader.Setup(a => a.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(a => a.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            agent.OnSchedule();
        }

        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

            //program.Main(null);
        }

        //public static string Tenant => Environment.GetEnvironmentVariable(TenantEnvironmentVariable) ?? "my-tenant";
        //public static string Schedule => Environment.GetEnvironmentVariable(ScheduleEnvironmentVariable) ?? "0 0/1 * 1/1 * ? *";
    }
}