﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Loans;
using LendFoundry.Notifications.Client;
using LendFoundry.Tasks.Loans.UpcomingPaymentNotification;
using Moq;
using Xunit;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Loans.CheckPaymentDueNotification.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Tasks.Tests.UpcomingPaymentNotification
{
    public class UpcomingPaymentNotificationTests : InMemoryObjects
    {
        public UpcomingPaymentNotificationTests()
        {
            ConfigurationFactory.Setup(x => x.Create<CheckPaymentDueConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            LoanServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(LoanService.Object);

            NotificationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(NotificationService.Object);

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);
        }

        private Mock<INotificationService> NotificationService { get; } = new Mock<INotificationService>();

        private NotificationSender NotificantionSender => new NotificationSender
            (
                LoanService.Object,
                NotificationService.Object,
                Logger.Object
            );

        private Loans.UpcomingPaymentNotification.Agent Agent => new Loans.UpcomingPaymentNotification.Agent
        (
            ConfigurationFactory.Object,
            TokenHandler.Object,
            LoanServiceFactory.Object,
            NotificationFactory.Object,
            TenantTimeFactory.Object,
            LoggerFactory.Object
        );

        [Fact]
        public void OnSchedule_Test()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("rockloans");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            LoanServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            NotificationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            TenantTime.Verify(x => x.Today);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OnSchedule_When_RaisedException()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("rockloans");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Throws<ArgumentNullException>();

            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            Agent.OnSchedule();

            // Assert
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            LoanServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            NotificationFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeastOnce);
        }


        [Fact]
        public void SendNotifications_When_ExecutionOk()
        {
            // arrange
            var installments = new[]
            {
                InMemoryInstallmentInfo,
                InMemoryInstallmentInfo,
                InMemoryInstallmentInfo
            };
            installments = installments.Select((installment, i) =>
            {
                installment.Loan.ReferenceNumber = $"loan00{i}";

                return installment;
            }).ToArray();

            LoanService.Setup(
                x =>
                    x.GetInstallmentsDueInByAnniversaryDate(
                        It.IsAny<DateTime>(), 
                        It.IsAny<PaymentMethod>(),
                        It.IsAny<InstallmentType>())).Returns(installments);

            Logger.Setup(x => x.Info(It.IsAny<string>()));

            // act
            NotificantionSender.SendNotifications(PaymentMethod.ACH, new DateTimeOffset(new DateTime(2016, 03, 20)));

            // assert
            LoanService.Verify(
                x =>
                    x.GetInstallmentsDueInByAnniversaryDate(
                        It.IsAny<DateTime>(),
                        It.IsAny<PaymentMethod>(),
                        It.IsAny<InstallmentType>()), Times.Once);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(4));
        }

        [Fact]
        public void SendNotifications_When_HasNoInstallments()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                LoanService.Setup(
                    x =>
                        x.GetInstallmentsDueInByAnniversaryDate(
                            It.IsAny<DateTime>(),
                            It.IsAny<PaymentMethod>(),
                            It.IsAny<InstallmentType>()));

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                // act
                NotificantionSender.SendNotifications(PaymentMethod.ACH, new DateTimeOffset(new DateTime(2016, 03, 20)));

                // assert
                LoanService.Verify(
                    x =>
                        x.GetInstallmentsDueInByAnniversaryDate(
                            It.IsAny<DateTime>(),
                            It.IsAny<PaymentMethod>(),
                            It.IsAny<InstallmentType>()), Times.Once);

                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeast(1));
            });
        }

        [Fact]
        public void SendNotifications_When_ExecutionFailed()
        {
            // arrange
            LoanService.Setup(
                x =>
                    x.GetInstallmentsDueInByAnniversaryDate(
                        It.IsAny<DateTime>(),
                        It.IsAny<PaymentMethod>(),
                        It.IsAny<InstallmentType>())).Returns(new List<IInstallmentInfo>());

            Logger.Setup(x => x.Info(It.IsAny<string>()));

            // act
            NotificantionSender.SendNotifications(PaymentMethod.ACH, new DateTimeOffset(new DateTime(2016, 03, 20)));

            // assert
            LoanService.Verify(
                x =>
                    x.GetInstallmentsDueInByAnniversaryDate(
                        It.IsAny<DateTime>(),
                        It.IsAny<PaymentMethod>(),
                        It.IsAny<InstallmentType>()), Times.Once);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(1));
        }


        [Fact]
        public void CheckNotificationRequestFactory_Test()
        {
            var requestFactory = new CheckNotificationRequestFactory();
            Assert.NotNull(requestFactory);

            var installmentInfo = InMemoryInstallmentInfo;
            installmentInfo.Installment.PaymentMethod = PaymentMethod.Check;
            installmentInfo.Loan.ReferenceNumber = "Loan001";
            
            var notificationRequest = requestFactory.Create(installmentInfo);

            Assert.NotNull(notificationRequest);
            Assert.NotNull(notificationRequest.Data);
            Assert.NotNull(notificationRequest.Recipient);
            Assert.Equal("Loan001", notificationRequest.Recipient.LoanReferenceNumber);
        }


        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

            //program.Main(null);
        }
    }
}