﻿using LendFoundry.Configuration.Client;
using LendFoundry.DocumentManager;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.SyndicationStore.Events;
using LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration;
using LendFoundry.TemplateManager;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using LendFoundry.DocumentGenerator;
using Xunit;
using Document = LendFoundry.DocumentManager.Document;
using IDocument = LendFoundry.DocumentManager.IDocument;
using IDocumentGeneratorServiceFactory = LendFoundry.DocumentGenerator.Client.IDocumentGeneratorServiceFactory;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled.Tests
{
    public class SyndicationCalledAgentTests
    {
        private Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        private Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        private Mock<IEventHubClientFactory> EventHubFactory { get; } = new Mock<IEventHubClientFactory>();

        private Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        private Mock<IConfigurationServiceFactory<SyndicationCalledConfiguration>> ConfigurationServiceFactory { get; } =
            new Mock<IConfigurationServiceFactory<SyndicationCalledConfiguration>>();

        private Mock<IConfigurationService<SyndicationCalledConfiguration>> ConfigurationService { get; } =
            new Mock<IConfigurationService<SyndicationCalledConfiguration>>();

        private Mock<IDocumentGeneratorServiceFactory> DocumentGeneratorFactory { get; } =
            new Mock<IDocumentGeneratorServiceFactory>();

        private Mock<IDocumentGeneratorService> DocumentGeneratorClient { get; } =
            new Mock<IDocumentGeneratorService>();

        private Mock<IDocumentManagerServiceFactory> DocumentManagerFactory { get; } =
            new Mock<IDocumentManagerServiceFactory>();

        private Mock<IDocumentManagerService> DocumentManagerService { get; } =
            new Mock<IDocumentManagerService>();

        private SyndicationCalledAgent Agent => new SyndicationCalledAgent
        (
            TokenHandler.Object,
            ConfigurationServiceFactory.Object,
            EventHubFactory.Object,
            LoggerFactory.Object,
            DocumentGeneratorFactory.Object,
            DocumentManagerFactory.Object
        );

        private SyndicationCalledConfiguration SyndicationCalledConfiguration
            => new SyndicationCalledConfiguration
            {
                Syndications = new Dictionary<string, List<SyndicationSubscription>>
                {
                    {
                        "application",
                        new List<SyndicationSubscription>
                        {
                            new SyndicationSubscription
                            {
                                SyndicationName = "CreditReport",
                                TemplateName = "test.template",
                                TemplateVersion = "v1",
                                Document = new DocumentConfiguration
                                {
                                    Name = "TestDocument",
                                }
                            }
                        }
                    }
                }
            };

        public SyndicationCalledAgentTests()
        {
            Logger.Setup(x => x.Warn(It.IsAny<string>()));

            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHubClient.Object);

            ConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            DocumentGeneratorFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DocumentGeneratorClient.Object);

            DocumentManagerFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DocumentManagerService.Object);
        }

        [Fact]
        public void Agent_OnExecute_HasEmptyConfiguration()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get());

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_HasConfiguration_NullOrEmptyEntityTypes()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(new SyndicationCalledConfiguration());

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_HasValidConfiguration_EntityTypeMissing()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(new SyndicationCalledConfiguration
                {
                    Syndications = new Dictionary<string, List<SyndicationSubscription>>()
                });

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_HasValidConfiguration()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(SyndicationCalledConfiguration);

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
        }



        [Fact]
        public void Agent_OnExecute_WithValidConfiguration_CallsDocumentGeneratorAndManager()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(SyndicationCalledConfiguration);

            Agent.Execute(@event);

            //LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeast(1));
            //TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            //ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            //DocumentGeneratorFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            //DocumentManagerFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            //DocumentGeneratorClient.Verify(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()));
            //DocumentManagerService.Verify(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()));
        }

        [Fact]
        public void Agent_OnExecute_WithValidConfiguration_GenerateDocumentThrowsException()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(SyndicationCalledConfiguration);

            DocumentGeneratorClient.Setup(
                x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()))
                .Throws(new ClientException("Request", new Error("Error occured")));

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            DocumentGeneratorFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            DocumentManagerFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            DocumentGeneratorClient.Verify(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()));
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_WithValidConfiguration_CreateDocumentThrowsException()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(SyndicationCalledConfiguration);

            DocumentGeneratorClient.Setup(
                x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()))
                .Returns(Task.FromResult<IDocumentResult>(new DocumentResult {Content = new byte[] { 1, 2, 3, 4, 5 } }));

            DocumentManagerService.Setup(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
                .Throws(new ClientException("Request", new Error("Error occured")));

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            DocumentGeneratorFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            DocumentManagerFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            DocumentGeneratorClient.Verify(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()));
            DocumentManagerService.Verify(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()));
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_WithValidConfiguration_WhenGeneratedAndCreatedDocument_OnSuccessRaisesEvent()
        {
            var @event = new EventInfo { Data = new SyndicationCalledEvent { EntityType = "application", EntityId = "CR000-001", Name = "CreditReport" }, TenantId = "my-tenant" };

            ConfigurationService.Setup(x => x.Get())
                .Returns(SyndicationCalledConfiguration);

            DocumentGeneratorClient.Setup(
                x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()))
                .Returns(Task.FromResult<IDocumentResult>(new DocumentResult { Content = new byte[] { 1, 2, 3, 4, 5 } }));

            DocumentManagerService.Setup(
                x =>
                    x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<List<string>>()))
                .Returns(Task.FromResult<IDocument>(new Document
                {
                    FileName = "TestDocument.v1.pdf",
                    Id = "TestId",
                    Size = 5,
                    Timestamp = DateTimeOffset.Now
                }));

            EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .Returns(Task.FromResult(true));

            Agent.Execute(@event);

            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeast(1));
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeast(1));
            ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            DocumentGeneratorFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            DocumentManagerFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            DocumentGeneratorClient.Verify(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()));
            DocumentManagerService.Verify(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()));
            EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            EventHubClient.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeast(1));
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
        }


        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);
        }
    }
}