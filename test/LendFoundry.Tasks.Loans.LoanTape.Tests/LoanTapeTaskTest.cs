﻿using LendFoundry.Applicant;
using LendFoundry.Application;
using LendFoundry.Application.Client;
using LendFoundry.Applications.Filters;
using LendFoundry.Applications.Filters.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DataAttributes;
using LendFoundry.DataAttributes.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Merchant;
using LendFoundry.Merchant.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Tests.Common;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Tasks.Loans.LoanTape.Tests
{
    public class LoanTapeTaskTests : InMemoryObjects
    {
        protected Mock<ILookupClientFactory> LookupClientFactory { get; } = new Mock<ILookupClientFactory>();
        protected Mock<ICalendarServiceFactory> CalendarServiceFactory { get; } = new Mock<ICalendarServiceFactory>();


        protected Mock<IConfigurationServiceFactory> ConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory>();

        protected new Mock<IConfigurationService<TaskConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<TaskConfiguration>>();

        protected Mock<IApplicationServiceClientFactory> ApplicationServiceClientFactory { get; } = new Mock<IApplicationServiceClientFactory>();

        protected Mock<IApplicationFilterService> ApplicationFilterService { get; } = new Mock<IApplicationFilterService>();

        protected Mock<IApplicationFilterClientFactory> ApplicationFilterClientFactory { get; } = new Mock<IApplicationFilterClientFactory>();

        protected Mock<IApplicationService> ApplicationService { get; } = new Mock<IApplicationService>();

        protected Mock<IDataAttributesClientFactory> DataAttributesFactory { get; } = new Mock<IDataAttributesClientFactory>();

        protected Mock<IDataAttributesEngine> DataAttributesService { get; } = new Mock<IDataAttributesEngine>();

        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();

        protected Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();

        protected Mock<IMerchantServiceFactory> MerchantServiceFactory { get; } = new Mock<IMerchantServiceFactory>();

        protected Mock<IMerchantService> MerchantService { get; } = new Mock<IMerchantService>();

        protected Mock<ITapeRepositoryFactory> TapeRepositoryFactory { get; } = new Mock<ITapeRepositoryFactory>();

        protected Mock<ITapeRepository> TapeRepository { get; } = new Mock<ITapeRepository>();

        protected Mock<ITokenReaderFactory> TokenReaderFactory { get; } = new Mock<ITokenReaderFactory>();
        protected Mock<ILookupService> LookupService { get; } = new Mock<ILookupService>();
        protected Mock<ICalendarService> CalendarService { get; } = new Mock<ICalendarService>();

        private Agent LoanTapeAgent => new Agent
        (
            ConfigurationServiceFactory.Object,
            TokenHandler.Object,
            TenantTimeFactory.Object,
            LoggerFactory.Object,
            ApplicationServiceClientFactory.Object,
            DataAttributesFactory.Object,
            EventHubClientFactory.Object,
            MerchantServiceFactory.Object,
            TapeRepositoryFactory.Object,
            TokenReaderFactory.Object,
            ApplicationFilterClientFactory.Object,
            LookupClientFactory.Object,
            CalendarServiceFactory.Object
        );

        public List<IApplicant> DummyApplicants = new List<IApplicant>
        {
            new Applicant.Applicant
            {
                Id = "123",
                Email = "nayan.paregi@gmail.com",
                FirstName = "Nayan",
                LastName = "Paregi",
                MiddleName = "G",
                Ssn = "123456789",
                DateOfBirth = new DateTime(1978, 8, 14)
            },
            new Applicant.Applicant
            {
                Id = "456",
                Email = "nayan.paregi@gmail.com",
                FirstName = "Nayan",
                LastName = "Paregi",
                MiddleName = "G",
                Ssn = "123456789",
                DateOfBirth = new DateTime(1978, 8, 14)
            }
        };

        public List<IApplication> DummyApplications = new List<IApplication>
        {
            new Application.Application
            {
                Id = "123",
                ApplicationNumber = "0000004",
                Status = new Application.Status { Code = "100.01", Name = "New" },
                Applicants = new List<IApplicantData>
                {
                    new ApplicantData
                    {
                        ApplicantId = "123",
                        CurrentEmployer = new Employer
                        {
                            Name = "Gateway",
                            Address = new Address
                            {
                                City = "Ahmedabad",
                                Country = "India",
                                IsPrimary = true
                            }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "9825820589", IsPrimary = true }
                        },
                        IsPrimary = true
                    }
                }
            }
        };

        public IApplicationResponse DummyApplicationResponse = new ApplicationResponse
        {
            Applicants = new List<Application.IApplicantRequest>
                {
                    new Application.ApplicantRequest
                    {
                        Id = "123",
                        Email = "nayan.paregi@gmail.com",
                        FirstName = "Nayan",
                        LastName = "Paregi",
                        MiddleName = "G",
                        Ssn = "123456789",
                        DateOfBirth = new DateTime(1978, 8, 14),
                        Addresses = new List<IAddress> {
                            new Address
                            {
                                City = "Ahmedabad",
                                Country = "India",
                                IsPrimary = true
                            }
                        },
                        PhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber { Number = "9825820589", IsPrimary = true }
                        },
                        IsPrimary = true,
                        TenantId = "my-tenant",
                        CurrentEmployer = new Employer
                        {
                            Name = "Gateway",
                            Address = new Address
                            {
                                City = "Ahmedabad",
                                Country = "India",
                                IsPrimary = true
                            },
                            LengthOfEmploymentInMonths = 3,
                            IsVerified = true,
                            PhoneNumber = "9825820589"
                        }
                    }
                },
            ApplicationNumber = "00004",
            Amount = 500000000,
            Purpose = "qwertyuiop",
            Scores = new[] { new Score { Date = DateTimeOffset.Now, Name = "name", Value = "value" } },
            Source = new Source { Id = "123", Type = SourceType.Merchant },
        };

        public IMerchant DummyMerchant = new Merchant.Merchant
            {
                Industry = new Industry { Code = "1001" },
                AnnualSales = 50000,
                BankAccounts = new[]
                {
                    new Merchant.BankAccount
                    {
                        AccountNumber = "123",
                        AccountType = AccountType.Checking,
                        IsPrimary = true,
                        RoutingNumber = "123",
                        AccountHolderName ="AccountHolderName"
                    }
                }.ToList<Merchant.IBankAccount>(),
                BankruptcyCount = 0,
                BusinessAddresses = new[]
                {
                    new BusinessAddress
                    {
                        City = "Irvine",
                        Country = "USA",
                        Line1 = "Line1",
                        State = "CA",
                        Type = BusinessAddressType.Home,
                        ZipCode = "96145"
                    }
                },
                BusinessLicences = new[]
                {
                    new BusinessLicense
                    {
                        Description = "Description",
                        ExpiredDate = new DateTimeOffset().AddDays(30),
                        IssuedDate = new DateTimeOffset().AddYears(1),
                        Issuer = "Issuer",
                        Number = "Number"
                    }
                },
                BusinessName = "BusinessName",
                BusinessType = new KeyValuePair<string, string>("Type", "Code"),
                Contacts = new[]
                {
                    new Contact
                    {
                        JobTitle = "JobTitle",
                        FirstName = "FirstName",
                        LastName = "LastName",
                        PhoneExt = "022",
                        PhoneNumber = "1234567890",
                        Scores = new Dictionary<string, string>()
                    }
                },
                Dba = "Dba",
                Email = "merchant1@email.com",
                ExecutiveCount = 0,
                FederalTaxIdOrSSN = "TaxId1",
                ForeclosureOrNoticeOfDefaultCount = 0,
                JudgementAndLienFilingCount = 0,
                NAICSCode = new KeyValuePair<string, string>("type", "code"),
                SICCode = new KeyValuePair<string, string>("type", "code"),
                TimeInBusiness = "10",
                UCCActiveFilingCount = 0,
                Website = "Website",
                YearBusinessStarted = 1986,
                UserId = "userId"
            };

        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);
        }

        [Fact]
        public void Agent_OnExecute_OnSuccess()
        {
            var @event = new EventInfo { Data = new ApplicationCreditPackageCreated { ApplicationNumber = "0000004" } };
            var taskConfiguration = new TaskConfiguration();

            var tokenReader = Mock.Of<ITokenReader>();
            TokenReaderFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(tokenReader);

            // Logger
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);
            
            // Configuration
            ConfigurationService.Setup(x => x.Get())
                .Returns(taskConfiguration);
            ConfigurationServiceFactory.Setup(x => x.Create<TaskConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);
            
            // TenantTime
            TenantTime.Setup(x => x.Now)
                .Returns(DateTimeOffset.Now);
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);
            // Lookup
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()))
               .Returns(new Dictionary<string, string> { { "automotive Program", "99" } });
            LookupClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(LookupService.Object);

            // Calendar
            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
               .Returns(new DateInfo { Date = DateTime.Now });
            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(CalendarService.Object);
            // ApplicationService
            var applicationResponse = DummyApplicationResponse;
            applicationResponse.Applicants.ForEach(a => 
            {
                a.IsPrimary = true;                
            });
            applicationResponse.SubmittedDate = new Foundation.Date.TimeBucket(DateTimeOffset.Now);

            ApplicationService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>()))
                .Returns(applicationResponse);
            ApplicationServiceClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationService.Object);
            
            // ApplicationFilters
            ApplicationFilterService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(Mock.Of<IFilterView>());
            ApplicationFilterClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);
            
            // DataAttributes
            var dataAttributes = new Dictionary<string, object>();
            dataAttributes.Add("ApprovedDated", DateTime.Now);
            dataAttributes.Add("NoteDate", DateTime.Now);
            dataAttributes.Add("FirstPaymentDueDate", DateTime.Now);
            dataAttributes.Add("ApprovedAmount", 2000);
            dataAttributes.Add("PayoutAmount", 3000);
            dataAttributes.Add("InstallmentAmount", 1000);
            dataAttributes.Add("OriginationFee", 2.5);
            dataAttributes.Add("RateAmount", 5);
            dataAttributes.Add("APR", 32.23);
            dataAttributes.Add("Term", 1.5);
            dataAttributes.Add("FicoScore", 111);
            dataAttributes.Add("FicoDate", DateTime.Now);
            dataAttributes.Add("RiskBand", "RiskBand");
            dataAttributes.Add("DebtUtilization", "DebtUtilization");
            dataAttributes.Add("TotalRevolvingDebt", "TotalRevolvingDebt");
            dataAttributes.Add("CreditInquiries12months", "CreditInquiries12months");
            dataAttributes.Add("DeliquencyPast24months", "DeliquencyPast24months");
            dataAttributes.Add("AccountsOpenedPast24months", "AccountsOpenedPast24months");
            dataAttributes.Add("OpenCreditLines", "OpenCreditLines");
            dataAttributes.Add("CollectionsExcludingMedical", "CollectionsExcludingMedical");
            dataAttributes.Add("PublicRecordsOnFile", "PublicRecordsOnFile");
            dataAttributes.Add("MonthsSinceLastRecord", "MonthsSinceLastRecord");
            dataAttributes.Add("PostDTI", 21.21);


            DataAttributesService.Setup(x => x.GetAllAttributes(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(dataAttributes);
            DataAttributesFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DataAttributesService.Object);
            
            // EventHubClient
            EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            EventHubClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHubClient.Object);
            
            // MerchantService
            MerchantService.Setup(x => x.Get(It.IsAny<string>())).
                Returns(DummyMerchant);
            MerchantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(MerchantService.Object);

            // TapeRepository
            TapeRepository.Setup(x => x.IsInProcess(It.IsAny<string>()))
                .Returns(false);
            TapeRepository.Setup(x => x.Add(It.IsAny<IFundingTape>()));
            TapeRepositoryFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(TapeRepository.Object);

            LoanTapeAgent.Execute(@event);

            TokenReaderFactory.Verify(x => x.Create(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeast(1));

            TapeRepository.Verify(x => x.Add(It.IsAny<IFundingTape>()));    
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void Agent_OnExecute_Applicant_WasNotFound()
        {
            var @event = new EventInfo { Data = new ApplicationCreditPackageCreated { ApplicationNumber = "0000004" } };
            var taskConfiguration = new TaskConfiguration();

            var tokenReader = Mock.Of<ITokenReader>();
            TokenReaderFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(tokenReader);

            // Logger
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);

            // Configuration
            ConfigurationService.Setup(x => x.Get())
                .Returns(taskConfiguration);
            ConfigurationServiceFactory.Setup(x => x.Create<TaskConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            // TenantTime
            TenantTime.Setup(x => x.Now)
                .Returns(DateTimeOffset.Now);
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // ApplicationService
            ApplicationService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>()))
                .Returns(new ApplicationResponse { ApplicationNumber = "0000004" });
            ApplicationServiceClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationService.Object);

            // ApplicationFilters
            ApplicationFilterService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(Mock.Of<IFilterView>());
            ApplicationFilterClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);

            // DataAttributes
            DataAttributesService.Setup(x => x.GetAllAttributes(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new Dictionary<string, object>());
            DataAttributesFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DataAttributesService.Object);

            // EventHubClient
            EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            EventHubClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHubClient.Object);

            // MerchantService
            MerchantService.Setup(x => x.Get(It.IsAny<string>())).
                Returns(Mock.Of<IMerchant>());
            MerchantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(MerchantService.Object);

            // TapeRepository
            TapeRepository.Setup(x => x.IsInProcess(It.IsAny<string>()))
                .Returns(false);
            TapeRepository.Setup(x => x.Add(It.IsAny<IFundingTape>()));
            TapeRepositoryFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(TapeRepository.Object);

            LoanTapeAgent.Execute(@event);

            TokenReaderFactory.Verify(x => x.Create(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeast(1));
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeast(2));
        }

        [Fact]
        public void Agent_OnExecute_Without_Merchant()
        {
            var @event = new EventInfo { Data = new ApplicationCreditPackageCreated { ApplicationNumber = "0000004" } };
            var taskConfiguration = new TaskConfiguration();

            var tokenReader = Mock.Of<ITokenReader>();
            TokenReaderFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(tokenReader);

            // Logger
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);

            // Configuration
            ConfigurationService.Setup(x => x.Get())
                .Returns(taskConfiguration);
            ConfigurationServiceFactory.Setup(x => x.Create<TaskConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);
            // Lookup
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(),It.IsAny<string>()))
               .Returns(new Dictionary<string, string> { { "automotive Program", "99" } });
            LookupClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(LookupService.Object);

            // Calendar
            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
               .Returns(new DateInfo { Date = DateTime.Now });
            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(CalendarService.Object);

            // TenantTime
            TenantTime.Setup(x => x.Now)
                .Returns(DateTimeOffset.Now);
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // ApplicationService
            var applicationResponse = DummyApplicationResponse;
            applicationResponse.Applicants.ForEach(a =>
            {
                a.IsPrimary = true;
            });
            applicationResponse.Source.Type = SourceType.Merchant;

            ApplicationService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>()))
                .Returns(DummyApplicationResponse);
            ApplicationServiceClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationService.Object);
          MerchantServiceFactory  .Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(MerchantService.Object);

            // ApplicationFilters
            ApplicationFilterService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(Mock.Of<IFilterView>());
            ApplicationFilterClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);

            // DataAttributes
            DataAttributesService.Setup(x => x.GetAllAttributes(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new Dictionary<string, object>());
            DataAttributesFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DataAttributesService.Object);

            // EventHubClient
            EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            EventHubClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHubClient.Object);

           
            // TapeRepository
            TapeRepository.SetupSequence(x => x.IsInProcess(It.IsAny<string>()))
                .Returns(false);
            TapeRepository.Setup(x => x.Add(It.IsAny<IFundingTape>()));
            TapeRepositoryFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(TapeRepository.Object);

            LoanTapeAgent.Execute(@event);

            TokenReaderFactory.Verify(x => x.Create(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeast(1));

            MerchantService.Verify(x => x.Get(It.IsAny<string>()), Times.AtLeastOnce);
            TapeRepository.Verify(x => x.Add(It.IsAny<IFundingTape>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void Agent_OnExecute_Without_Some_DateTime_Fields()
        {
            var @event = new EventInfo { Data = new ApplicationCreditPackageCreated { ApplicationNumber = "0000004" } };
            var taskConfiguration = new TaskConfiguration();

            var tokenReader = Mock.Of<ITokenReader>();
            TokenReaderFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(tokenReader);

            // Logger
            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.CreateLogger())
                .Returns(Logger.Object);

            // Configuration
            ConfigurationService.Setup(x => x.Get())
                .Returns(taskConfiguration);
            ConfigurationServiceFactory.Setup(x => x.Create<TaskConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            // TenantTime
            TenantTime.Setup(x => x.Now)
                .Returns(DateTimeOffset.Now);
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            // ApplicationService
            var applicationResponse = DummyApplicationResponse;
            applicationResponse.Applicants.ForEach(a =>
            {
                a.IsPrimary = true;
            });
            applicationResponse.Source.Type = SourceType.Merchant;

            ApplicationService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>()))
                .Returns(DummyApplicationResponse);
            ApplicationServiceClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationService.Object);
            MerchantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(MerchantService.Object);

            // ApplicationFilters
            ApplicationFilterService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(Mock.Of<IFilterView>());
            ApplicationFilterClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(ApplicationFilterService.Object);

            // DataAttributes
            DataAttributesService.Setup(x => x.GetAllAttributes(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new Dictionary<string, object>());
            DataAttributesFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(DataAttributesService.Object);

            // Lookup
            LookupService.Setup(x => x.GetLookupEntry(It.IsAny<string>(),It.IsAny<string>()))
               .Returns(new Dictionary<string, string> { { "automotive Program", "99" } });
            LookupClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(LookupService.Object);

            // Calendar
            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
               .Returns(new DateInfo { Date = DateTime.Now });
            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
               .Returns(CalendarService.Object);
            // EventHubClient
            EventHubClient.Setup(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()))
                .ReturnsAsync(true);
            EventHubClientFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(EventHubClient.Object);

            // MerchantService
            MerchantService.Setup(x => x.Get(It.IsAny<string>())).
                Returns(DummyMerchant);
            MerchantServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(MerchantService.Object);

            // TapeRepository
            TapeRepository.SetupSequence(x => x.IsInProcess(It.IsAny<string>()))
                .Returns(false);
            TapeRepository.Setup(x => x.Add(It.IsAny<IFundingTape>()));
            TapeRepositoryFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(TapeRepository.Object);

            LoanTapeAgent.Execute(@event);

            TokenReaderFactory.Verify(x => x.Create(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.CreateLogger(), Times.AtLeast(1));

            //ConfigurationService.Verify(x => x.Get(), Times.AtLeast(1));
            //DocumentGeneratorFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            //DocumentManagerFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            //DocumentGeneratorClient.Verify(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), Format.Html, It.IsAny<DocumentGenerator.Document>()));
            //DocumentManagerService.Verify(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()));
            //EventHubFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeast(1));
            //EventHubClient.Verify(x => x.Publish(It.IsAny<string>(), It.IsAny<object>()), Times.AtLeast(1));
            TapeRepository.Verify(x => x.Add(It.IsAny<IFundingTape>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }
    }
}