﻿using LendFoundry.Application;
using LendFoundry.Application.Client;

using LendFoundry.Configuration.Client;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;

using LendFoundry.Security.Tokens;
using LendFoundry.TemplateManager;

using Moq;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Tasks.Applications.CreateCreditPackage.Tests
{
    public class FakeDocumentGeneratorService : IDocumentGeneratorService
    {
        public FakeDocumentGeneratorService()
        {
        }

        public Task<IDocumentResult> Generate(DocumentFormat format, IDocumentUrl documentUrl)
        {
            throw new NotImplementedException();
        }

        public Task<IDocumentResult> Generate(string templateName, string version, Format format, DocumentGenerator.IDocument document)
        {
            if(templateName== "NullContentCertificateOfCompletionTemplate")
            return  Task.FromResult<IDocumentResult>(new DocumentResult { Content = null, DocumentName = "test.pdf" });
            else if (templateName == "CertificateOfCompletionTemplateContentLengthZero")
                return Task.FromResult<IDocumentResult>(new DocumentResult { Content = System.Text.Encoding.ASCII.GetBytes(""), DocumentName = "test.pdf" });
            return Task.FromResult<IDocumentResult>(new DocumentResult { Content = System.Text.Encoding.ASCII.GetBytes("whatever"), DocumentName = "test.pdf" });
        }
    }

    public class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        // ------------------------- Configuration Objects
        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();

        protected Mock<IConfigurationService<Configuration>> ConfigurationService { get; } = new Mock<IConfigurationService<Configuration>>();

        // ------------------------- TenantTime Objects
        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        protected Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>();

        protected Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();

        protected Mock<ITokenReaderFactory> TokenReaderFactory { get; } = new Mock<ITokenReaderFactory>();

        protected Mock<ITokenReader> TokenReader { get; } = new Mock<ITokenReader>();

        // ------------------------- EventHub Objects
        protected Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();

        protected Mock<IEventHubClient> EventHubClient { get; } = new Mock<IEventHubClient>();


        // ------------------------- Logger Objects
        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        protected Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();

      
        // ------------------------- Application Objects
        protected Mock<IApplicationService> ApplicationService { get; } = new Mock<IApplicationService>();

        protected Mock<IApplicationServiceClientFactory> ApplicationServiceClientFactory { get; } = new Mock<IApplicationServiceClientFactory>();

        // ------------------------- Document Manager Objects
        protected Mock<IDocumentManagerService> DocumentManagerService { get; } = new Mock<IDocumentManagerService>();

        protected Mock<IDocumentManagerServiceFactory> DocumentManagerServiceFactory { get; } = new Mock<IDocumentManagerServiceFactory>();

        // ------------------------- Document Generator Objects
        protected IDocumentGeneratorService DocumentGeneratorService { get; } = new FakeDocumentGeneratorService();

        protected Mock<DocumentGenerator.Client.IDocumentGeneratorServiceFactory> DocumentGeneratorServiceFactory { get; } = new Mock<DocumentGenerator.Client.IDocumentGeneratorServiceFactory>();

        protected Mock<IPdfMergerService> PdfMergerService { get; } = new Mock<IPdfMergerService>();
    }
}