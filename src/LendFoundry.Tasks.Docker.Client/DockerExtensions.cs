using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks.Docker.Client
{
    public static class DockerExtensions
    {
        public static IServiceCollection AddDockerRunner(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ITaskRunnerFactory>(p => new DockerRunnerFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITaskRunnerFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}