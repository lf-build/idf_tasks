using LendFoundry.Tasks.Agent;
using Quartz;

namespace LendFoundry.Tasks.Dispatcher
{
    public class SimpleJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            var runner = context.MergedJobDataMap.Get("runner") as ITaskRunner;
            var task = context.MergedJobDataMap.Get("task") as ITask;
            if (task == null)
                return;

            //if (task.Environment.ContainsKey(ScheduledAgent.TenantEnvironmentVariable))
            //    task.Environment.Remove(ScheduledAgent.TenantEnvironmentVariable);
            //task.Environment.Add(ScheduledAgent.TenantEnvironmentVariable, task.Tenant);
            runner?.Run(task);
        }
    }
}