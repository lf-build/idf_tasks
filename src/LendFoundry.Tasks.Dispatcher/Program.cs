﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Dispatcher.Configuration;
using LendFoundry.Tasks.Docker.Client;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks.Dispatcher
{
    public class Program : DependencyInjection
    {
        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddSingleton<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddDockerRunner(Settings.Docker.Host, Settings.Docker.Port);
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats);
            services.AddConfigurationService<TaskConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddSingleton<ITaskDispatcher, TaskDispatcher>();
            services.AddSingleton(p => p);
            return services;
        }

        public void Main(string[] args)
        {
            Provider.GetService<ITaskDispatcher>().Start();
        }
    }
}