﻿namespace LendFoundry.Tasks.Dispatcher
{
    public interface ITaskDispatcher
    {
        void Start();
    }
}