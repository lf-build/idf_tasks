﻿using System.Collections.Generic;

namespace LendFoundry.Tasks.Dispatcher.Configuration
{
    public interface ITaskConfiguration
    {
        List<Task> Definitions { get; set; }
    }
}