﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Dispatcher.Configuration;
using LendFoundry.Tasks.Docker.Client;
using LendFoundry.Tenant.Client;
using Quartz;
using Quartz.Impl;
using Task = LendFoundry.Tasks.Dispatcher.Configuration.Task;

namespace LendFoundry.Tasks.Dispatcher
{
    public class TaskDispatcher : ITaskDispatcher
    {
        public TaskDispatcher(IConfigurationServiceFactory configurationFactory, ITenantServiceFactory tenantServiceFactory, ITokenHandler tokenHandler, ITenantTimeFactory tenantTimeFactory, ITaskRunnerFactory taskRunnerFactory, IServiceProvider provider)
        {
            TenantServiceFactory = tenantServiceFactory;
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            TaskRunnerFactory = taskRunnerFactory;
            ServiceProvider = provider;
        }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITaskRunnerFactory TaskRunnerFactory { get; }

        private IServiceProvider ServiceProvider { get; }

        public void Start()
        {
            var scheduler = new StdSchedulerFactory().GetScheduler();
            scheduler.Start();
            var o = new object();

            var tenantService = TenantServiceFactory.Create(new StaticTokenReader(string.Empty));
            Parallel.ForEach(tenantService.GetActiveTenants(), tenant =>
            {
                var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                var configurationService = ConfigurationFactory.Create<TaskConfiguration>(Settings.ServiceName, reader);
                var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                var taskRunner = TaskRunnerFactory.Create(reader);
                try
                {
                    Parallel.ForEach(configurationService.Get().Definitions, task =>
                    {
                        task.Tenant = tenant.Id;
                        lock (o) scheduler.ScheduleJob(CreateJob(task, taskRunner), CreateTrigger(task, tenantTime));
                    });
                }
                catch (ClientException)
                {
                }
            });

            while (!scheduler.IsShutdown)
                Thread.Sleep(1000);
        }

        private IJobDetail CreateJob(Task task, ITaskRunner runner)
        {
            var data = new Dictionary<string, object>
            {
                {"task", task},
                {"runner", runner},
                {"p", ServiceProvider }
            } as IDictionary<string, object>;

            var builder = task.Type == TaskType.EventBased
                ? JobBuilder.Create<EventBasedJob>()
                : JobBuilder.Create<SimpleJob>();

            return builder.SetJobData(new JobDataMap(data)).Build();
        }

        private static ITrigger CreateTrigger(Task task, ITenantTime tenantTime)
        {
            var triggerBuilder = TriggerBuilder.Create();
            if (task.Type == TaskType.Scheduled)
            {
                var expression = new CronExpression(task.Expression) { TimeZone = tenantTime.TimeZone };
                var schedule = CronScheduleBuilder.CronSchedule(expression);
                triggerBuilder = triggerBuilder.WithSchedule(schedule);
            }
            return triggerBuilder.StartNow().Build();
        }
    }
}