using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Tasks.Dispatcher
{
    public static class Settings
    {
        public static string ServiceName => "tasks";

        public static readonly ServiceSettings EventHub      = new ServiceSettings("TASKS_EVENTHUB_HOST", "eventhub", "TASKS_EVENTHUB_PORT", 5000);

        public static readonly ServiceSettings Configuration = new ServiceSettings("TASKS_CONFIGURATION_HOST", "configuration", "TASKS_CONFIGURATION_PORT", 5000);

        public static readonly ServiceSettings Tenant        = new ServiceSettings("TASKS_TENANT_HOST", "tenant", "TASKS_TENANT_PORT", 5000);

        public static readonly ServiceSettings Docker        = new ServiceSettings("TASKS_DOCKER_HOST", "docker", "TASKS_DOCKER_PORT", 5000);

        public static string Nats => Environment.GetEnvironmentVariable("TASKS_NATS_URL") ?? "nats://nats:4222";
    }
}