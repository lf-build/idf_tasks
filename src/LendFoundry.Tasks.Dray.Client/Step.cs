using System.Collections.Generic;

namespace LendFoundry.Tasks.Dray.Client
{
    public class Step
    {
        public Step()
        {
            Environment = new List<EnvironmentVariable>();
        }

        public Step(string name) : this()
        {
            Name = name;
        }

        public string Name { get; set; }

        public string Source { get; set; }

        public List<EnvironmentVariable> Environment { get; set; }
    }
}