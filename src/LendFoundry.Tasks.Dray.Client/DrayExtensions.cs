using LendFoundry.Foundation.Services;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks.Dray.Client
{
    public static class DrayExtensions
    {
        public static IServiceCollection AddDrayClient(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ITaskRunner>(p => new Dray(services.GetServiceClient(p, endpoint, port)));
            return services;   
        }
    }
}