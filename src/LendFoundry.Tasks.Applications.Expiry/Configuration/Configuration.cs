namespace LendFoundry.Tasks.Applications.Expiry
{
    public class Configuration
    {
        public ExpirationMapping Expiration { get; set; }
    }
}