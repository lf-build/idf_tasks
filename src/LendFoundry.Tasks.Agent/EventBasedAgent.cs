﻿using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System.Threading;

namespace LendFoundry.Tasks.Agent
{
    public abstract class EventBasedAgent : IEventBasedAgent
    {
        private const string TenantEnvironmentVariable = "TASK_TENANT";

        private static readonly ManualResetEvent _waiter = new ManualResetEvent(false);

        protected EventBasedAgent(IEventHubClientFactory eventHubFactory, string eventName)
        {
            EventHubFactory = eventHubFactory;
            EventName = eventName;
            Tenant = Environment.GetEnvironmentVariable(TenantEnvironmentVariable);
        }

        public EventBasedAgent(IEventHubClientFactory eventHubFactory, string taskName, string eventName, ITokenHandler tokenHandler, ILoggerFactory loggerFactory) : this(eventHubFactory, eventName)
        {
            TaskName = taskName;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
        }

        protected IEventHubClientFactory EventHubFactory { get; }

        protected ITokenHandler TokenHandler { get; }

        protected ILoggerFactory LoggerFactory{ get; }

        private string TaskName { get; }

        private string EventName { get; }

        protected string Tenant { get; }

        public void Execute()
        {
            var logger = LoggerFactory.CreateLogger();
            try
            {
                logger.Info("Starting Agent");
                var token = TokenHandler.Issue(Tenant, TaskName);
                var hub = EventHubFactory.Create(new StaticTokenReader(token.Value));
                logger.Info($"Subscribing to {EventName} to {Tenant}...");
                hub.On(EventName, @event =>
                {
                    logger.Info($"Event {@event.Name} received from {@event.TenantId}, starting execution...", @event);
                    Execute(@event);
                    logger.Info("Execution completed");

                });
                hub.StartAsync();
                logger.Info("Subscription active");
                logger.Info("Agent started");
                Console.CancelKeyPress += (e, a) => _waiter.Set();
                _waiter.WaitOne();
                //logger.Info("Agent stopped");
                //hub.Stop();
            }
            catch (Exception ex)
            {
                logger.Error($"\nAgent raise an error: {ex.Message} \n", ex);
                Execute();
            }
        }

        public abstract void Execute(EventInfo @event);
    }
}