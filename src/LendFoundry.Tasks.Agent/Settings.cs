﻿using System;

namespace LendFoundry.Tasks.Agent
{
    public class Settings
    {
        private const string TenantEnvironmentVariable = "TASK_TENANT";

        private const string ScheduleEnvironmentVariable = "TASK_SCHEDULE";

        public static string Tenant  => Environment.GetEnvironmentVariable(TenantEnvironmentVariable) ?? "my-tenant";

        public static string Schedule  => Environment.GetEnvironmentVariable(ScheduleEnvironmentVariable) ?? "0 0/1 * 1/1 * ? *";
    }
}