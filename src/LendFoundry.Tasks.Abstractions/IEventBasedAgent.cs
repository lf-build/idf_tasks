
using LendFoundry.EventHub.Client;

namespace LendFoundry.Tasks
{
    public interface IEventBasedAgent : IAgent
    {
        
        void Execute(EventInfo @event);
    }
}