namespace LendFoundry.Tasks
{
    public interface IAgent
    {
        void Execute();
    }
}