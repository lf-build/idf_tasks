﻿using System;

namespace LendFoundry.Tasks
{
    public interface ISettingReader
    {
        string GetSetting(string name);
    }

    public class SettingReader : ISettingReader
    {
        public string GetSetting(string name)
        {
            return Environment.GetEnvironmentVariable(name);
        }
    }
}