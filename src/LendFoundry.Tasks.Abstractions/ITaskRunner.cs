using System.Threading.Tasks;

namespace LendFoundry.Tasks
{
    public interface ITaskRunner
    {
        Task<bool> Run(ITask task);
    }
}