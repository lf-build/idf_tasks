namespace LendFoundry.Tasks
{
    public class EnvironmentVariable
    {
        public EnvironmentVariable()
        {    
        }

        public EnvironmentVariable(string variable, string value)
        {
            Variable = variable;
            Value = value;
        }

        public string Variable { get; set; }

        public string Value { get; set; }
    }
}