using System;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Tasks
{
    public abstract class DependencyInjection
    {
        protected DependencyInjection()
        {
            Provider = ConfigureServices(new ServiceCollection()).BuildServiceProvider();
        }

        protected IServiceProvider Provider { get; }

        protected abstract IServiceCollection ConfigureServices(IServiceCollection services);
    }
}