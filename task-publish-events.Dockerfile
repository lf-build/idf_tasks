FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Tasks.Abstractions /app/LendFoundry.Tasks.Abstractions
WORKDIR /app/LendFoundry.Tasks.Abstractions
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Tasks.Agent /app/LendFoundry.Tasks.Agent
WORKDIR /app/LendFoundry.Tasks.Agent
RUN eval "$CMD_RESTORE"

ADD ./src/LendFoundry.Tasks.Loans.PublishEvents /app/LendFoundry.Tasks.Loans.PublishEvents
WORKDIR /app/LendFoundry.Tasks.Loans.PublishEvents
RUN eval "$CMD_RESTORE"
RUN dnu build

ENTRYPOINT dnx run