FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Tasks.Abstractions /app/LendFoundry.Tasks.Abstractions
WORKDIR /app/LendFoundry.Tasks.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/LendFoundry.Tasks.Agent /app/LendFoundry.Tasks.Agent
WORKDIR /app/LendFoundry.Tasks.Agent
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/Lendfoundry.Tasks.Ach.SendNocReceivedReport /app/Lendfoundry.Tasks.Ach.SendNocReceivedReport
WORKDIR /app/Lendfoundry.Tasks.Ach.SendNocReceivedReport
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ENTRYPOINT dnx run